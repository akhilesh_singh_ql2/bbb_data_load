import csv
import json
import logging
import os
import socket
import sys
from datetime import datetime

import db.connections as connections

if len(sys.argv) != 2:
    print('No config file parameter supplied. exiting')
    sys.exit(1)

now = datetime.now()
hostname = socket.gethostname()

log_date = now.strftime('%d-%m-%Y')

abs_path = os.path.dirname(os.path.abspath(__file__))

if 'rtl' in hostname:

    logging.basicConfig(filename='/u03/tires/log/bbb_data_load/bbb_data_load_{0}.log'.format(log_date),
                        level=logging.INFO,
                        format='%(asctime)s %(levelname)s:%(message)s',
                        filemode='w')
else:
    logging.basicConfig(filename='log/bbb_data_load_{0}.log'.format(log_date),
                        level=logging.INFO,
                        format='%(asctime)s %(levelname)s:%(message)s',
                        filemode='w')

try:
    with open(sys.argv[1]) as json_data_file:
        config = json.load(json_data_file)
except IOError:
    logging.error('Failed to load properties file: {0}'.format(sys.argv[1]))
    sys.exit(1)

try:
    limit = config['limit']
    occurrence = config['occurrence']
    occur = config['occur']
    db_user = config['postgres']['username']
    db_password = config['postgres']['password']
    db_host = config['postgres']['host']
    db_name = config['postgres']['db']
    db_tables = config['postgres']['tables']

    # Snowflake credential
    sf_user = config['snowflake']['username']
    sf_password = config['snowflake']['password']
    sf_host = config['snowflake']['host']
    sf_account = config['snowflake']['account']
    sf_db = config['snowflake']['db']
    sf_warehouse = config['snowflake']['warehouse']
    sf_table = config['snowflake']['tables']
    batch_size = config['batch_size']

    overwrite_tables = config['overwrite_tables']

    stage_table = config['snowflake']['stage_table']

    csv_path = config['csv_path']

except IOError:
    logging.error('Problem loading data from: {0}'.format(sys.argv[1]))


def delete_csv_files(path):
    for root, dirs, files in os.walk(path):
        for file in files:
            logging.info('Deleting file {}'.format(file))
            os.remove(os.path.join(root, file))


def update_occur(value=0):
    file_name = sys.argv[1]
    with open(file_name, 'r') as json_data_file:
        data = json.load(json_data_file)
        if value == 0:
            data['occur'] = 0
        else:
            data['occur'] = occur + value  # <--- add `id` value.

    jsonFile = open(file_name, "w+")
    jsonFile.write(json.dumps(data))
    jsonFile.close()


def initializing_sf_db(ctx):
    try:
        sql = "USE " + sf_db + " ;"
        cur = ctx.cursor()
        cur.execute(sql)

        sql = "USE WAREHOUSE {} ;".format(sf_warehouse)
        cur = ctx.cursor()
        cur.execute(sql)
    except Exception as e:
        logging.error(str(e))


# Truncate table on snowflake
def truncate_sf_tables(ctx, table):
    for v in overwrite_tables:
        if table == v.split('.')[1]:
            logging.info('Truncate of table {0}: STARTED'.format(table))
            sql = 'truncate table {0}'.format(table)
            cur = ctx.cursor()
            cur.execute(sql)
            logging.info('Truncate of table {0}: COMPLETED'.format(table))
            break


def get_max_match_id(ctx, table_name):
    initializing_sf_db(ctx)
    sql = 'select max(batch_id) from {}'.format(table_name)
    cur = ctx.cursor()
    id = cur.execute(sql).fetchone()
    logging.info('Max BATCH_ID is {}'.format(id[0]))
    return id[0]


# Fetching data from PSQL tables
def get_all_data(con, table):
    cursor = con.cursor('psql_cursor')
    query = 'select * from {}{}'.format(table, limit)
    if table == 'ra_bbb_rpt.bbb_fact_partition':
        batch_id = get_max_match_id(ctx=ctx, table_name=table.split('.')[1])

        if batch_id is not None:
            query = 'select * from {} where batch_id>{}{}'.format(table, batch_id, limit)

    if table == 'ra_bbb_rpt.bbb_active_product':
        batch_id = now.strftime('%Y%m%d%I0000')
        query = 'select {} as batch_id ,* from {}{}'.format(batch_id, table, limit)
    try:
        cursor.execute(query)
        while True:
            rows = cursor.fetchmany(batch_size)

            if not rows:
                break
            else:

                create_csv(table, rows)

        cursor.close()
    except Exception as e:
        logging.error('Error in get_all_data : {}'.format(e))


def append_csv(file_name, rows):
    with open(abs_path + os.sep + "csv/{}".format(file_name), "a", newline='', encoding="utf-8") as f:
        writer = csv.writer(f, delimiter=",")
        for row in rows:
            writer.writerow(row)


# Generating CSV files.
def create_csv(file_name, rows):
    file_name = file_name.split('.')[1] + '.csv'

    path = abs_path + os.sep + "csv/{}".format(file_name)

    if os.path.exists(path):
        append_csv(file_name, rows)
    else:
        logging.info('Generating CSV file {}'.format(file_name))
        with open(path, "w", encoding="utf-8", newline='') as f:
            writer = csv.writer(f, delimiter=",")
            for row in rows:
                writer.writerow(row)

    # print('file created {}'.format(abs_path + os.sep + "csv/{}".format(file_name)))


# Loading data into snowflake.
def load_in_snowflake(ctx, file_name):
    file_name = file_name.split('.')[1] + '.csv'
    logging.info('Loading to snowflake from: {}'.format(file_name))

    try:
        sql = "ALTER WAREHOUSE " + sf_warehouse + " resume;"
        cur = ctx.cursor()
        cur.execute(sql)
    except Exception as e:
        if 'since it is not suspended' not in str(e):
            logging.error(str(e))

    try:

        table_name = file_name.split('.')[0]

        sql = 'remove @{1}/{0}.csv.gz'.format(table_name, stage_table)
        cur = ctx.cursor()
        cur.execute(sql)

        csv_file = abs_path + os.sep + "csv/{}".format(file_name)

        # print('file reading{}'.format(abs_path + os.sep + "csv/{}".format(file_name)))
        sql = "put file://{0} @{1} auto_compress=true;".format(csv_file, stage_table)
        cur = ctx.cursor()
        cur.execute(sql)

        truncate_sf_tables(ctx=ctx, table=table_name)

        sql = "copy into {0} from @{1}/{0}.csv.gz FILE_FORMAT=(TYPE=csv FIELD_OPTIONALLY_ENCLOSED_BY = '\x01'  field_delimiter=',' ESCAPE_UNENCLOSED_FIELD='NONE'  skip_header=0 encoding='ISO-8859-1')" \
              " ON_ERROR = 'ABORT_STATEMENT' PURGE = TRUE;".format(table_name, stage_table)

        cur = ctx.cursor()
        cur.execute(sql)
    except Exception as e:
        logging.error('Error on Snowflake : {}'.format(e))


if __name__ == '__main__':
    logging.info('Process Started.')

    update_occur(1)
    logging.info('Getting PSQL connection objects')
    con = connections.get_psql_connection_obj(db_host=db_host, db_name=db_name, db_password=db_password,
                                              db_user=db_user, logging=logging)

    logging.info('Getting Snowflake connection objects')
    ctx = connections.get_sf_connection_obj(sf_user=sf_user, sf_password=sf_password, sf_account=sf_account,
                                            logging=logging)

    for table in db_tables:

        logging.info('Fetching data from table {}'.format(table))
        rows = get_all_data(con, table=table)

        logging.info('Loading data into snowflake in table {}'.format(table))
        load_in_snowflake(ctx, file_name=table)

        if occur == 1:
            break
        elif occurrence == occur + 1:
            update_occur(0)
            break

    logging.info('Closing connections')
    ctx.close()
    con.close()

    delete_csv_files(csv_path)

    logging.info('Process Completed.')
