import sys

import psycopg2
import snowflake.connector as sf


def get_psql_connection_obj(db_host, db_name, db_user, db_password, logging):
    global con, cursor

    try:
        con = psycopg2.connect(host=db_host, database=db_name, user=db_user, password=db_password)
        cursor = con.cursor('psql_cursor')
    except Exception as exception:
        logging.info('Failed to connect to database: {0}'.format(db_host))
        logging.info('Not connected to database: {0} as {1}'.format(db_host, exception))
        sys.exit(1)

    return con, cursor


def get_sf_connection_obj(sf_account, sf_user, sf_password, logging):
    try:
        ctx = sf.connect(user=sf_user, password=sf_password, account=sf_account)
    except Exception as exception:
        logging.info('Failed to connect to database: {0}'.format(sf_account))
        logging.info('Not connected to database: {0} as {1}'.format(sf_account, exception))
        sys.exit(1)

    return ctx


